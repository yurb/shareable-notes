# What is Tor and how to use it?

In short,

**If you think your browsing history is your private business** and believe no one should be able to spy on what you read and where you go on the net, you need [Tor](https://torproject.org/). Here's a neat [video](https://commons.wikimedia.org/wiki/File:Tor_Animation_en.webm) about it:

[![Video thumbnail](/images/tor-animation-thumbnail.png)](https://upload.wikimedia.org/wikipedia/commons/transcoded/a/a1/Tor_Animation_en.webm/Tor_Animation_en.webm.480p.webm)

_([download](https://commons.wikimedia.org/wiki/File:Tor_Animation_en.webm))_

## If you use Tor...
1. Your Internet provider is not able to see or record which sites you go to and what you do on the net;
2. Your Internet provider can't modify your traffic, block websites or inject ads/malware;
3. The sites you visit don't know where you're from, what other sites you've visited;
4. Ad networks and other trackers can't record your browsing history;
5. Your Internet provider and the websites can only see that you use Tor (there's [ways to avoid this](https://tor.stackexchange.com/a/3384) too, if you need).

## Keep in mind
1. Don't install plugins like Flash on Tor Browser, they'll leak information about you (youtube and many other sites work without flash);
2. Always install updates of the Tor Browser as soon as they appear (Tor will let you know), it's a [big deal](http://arstechnica.com/security/2013/08/attackers-wield-firefox-exploit-to-uncloak-anonymous-tor-users/);
3. Take care of your computer - if it has viruses, it can spy on what you do;
4. There's [more privacy tech](https://www.eff.org/deeplinks/2014/10/7-privacy-tools-essential-making-citizenfour) out there - don't be afraid to learn it. The more you know, the more safe and free you are.

## Tips for certain sites
Some sites treat Tor users badly. Here's how to fix it:
- Google. It loves to annoy Tor users with [captchas](https://en.wikipedia.org/wiki/CAPTCHA). To avoid it, just use the search box in the Tor Browser instead of going to google.com. You can also switch the search engine to [duckduckgo](https://duckduckgo.com/), which has no problem with Tor users.
- Gmail. Did I tell you I moved from Gmail a long time ago (even before I started using Tor)? Anyway, you can use Gmail with Tor if you first give Google your phone number in the settings. They will then send you a text message to verify it's you logging in. You decide if you want this.
- Other sites that annoy you with captchas. Well, sometimes there's no other way than entering the captcha. You may also try clicking "Use new Tor circuit for this site" under the onion icon if the captcha is too hard - maybe not all Tor IPs were blocked by that website.

## More info
- Details on [how Tor works](https://www.torproject.org/about/overview.html.en);
- A [rock-solid guide](https://freedom.press/encryption-works) about encryption from the Freedom of Press Foundation;
- [Surveillence Self-Defence](https://ssd.eff.org/) guide from the Electronic Frountier Foundation;
- Useful guides from The Intercept:
    - [Chatting in secret while we're all being watched](https://theintercept.com/2015/07/14/communicating-secret-watched/)
    - [Ed Snowden Taught Me To Smuggle Secrets Past Incredible Danger. Now I Teach You.](https://theintercept.com/2014/10/28/smuggling-snowden-secrets/)
- Videos:
    - TED: [Glenn Greenwald: Why privacy matters](http://www.ted.com/talks/glenn_greenwald_why_privacy_matters)
    - TEDxFlanders: [Jacob Appelbaum: The Tor Project, protecting online anonimity](https://www.youtube.com/watch?v=gCWeVYCcKXw)
    - TEDxMidAtlantic: [Alex Winter: The Dark Net isn't what you think. It's actually key to our privacy](https://www.youtube.com/watch?v=luvthTjC0OI)
    - [Tor Project: We're living in interesting times](https://www.youtube.com/watch?v=CJNxbpbHA-I)
