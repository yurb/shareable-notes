# Some context around CITIZENFOUR

## More about Edward Snowden
* TED interview: [Here's how we take back the Internet](https://www.ted.com/talks/edward_snowden_here_s_how_we_take_back_the_internet)
* How [Snowden communicated electronically](https://theintercept.com/2014/10/28/smuggling-snowden-secrets/)
* Privacy advice by Snowden [for more day-to-day situations](https://theintercept.com/2015/11/12/edward-snowden-explains-how-to-reclaim-your-privacy/)

## Key info about NSA spying programs
* Overview and introduction [from EFF](https://www.eff.org/nsa-spying)
* Detailed info [on Wikipedia](https://en.wikipedia.org/wiki/Global_surveillance_disclosures_(2013%E2%80%93present))

## Interesting slides

* [Upstream Collection](https://commons.wikimedia.org/wiki/File:Worldwide_NSA_signals_intelligence.jpg)
* [Xkeyscore map](https://commons.wikimedia.org/wiki/File:Xkeyscore-worldmap.jpg)
* [Xkeyscore screenshot](https://commons.wikimedia.org/wiki/File:KS7-001.jpg)
* [Collect it all](https://commons.wikimedia.org/wiki/File:Collectitall-diagram.jpg)

## Tools & tips
* EFF's [surveillance self-defence](https://ssd.eff.org) guide
* [Tor Browser](https://torproject.org)
* [TAILS](https://tails.boum.org) - complete operating system for running from USB
