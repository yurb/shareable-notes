# I'm quitting Facebook | Я йду з Facebook
_originally published at [diaspora](https://diasp.eu/posts/3230437)_

[![Red pill and blue pill](https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Red_and_blue_pill.jpg/256px-Red_and_blue_pill.jpg)](https://commons.wikimedia.org/wiki/File:Red_and_blue_pill.jpg#/media/File:Red_and_blue_pill.jpg)

_English version below._

Коли я створював обліковку на ФБ, я знав, що рано чи пізно її ліквідую. Мені було цікаво, як воно — бути на ФБ; тепер, коли мою цікавість цілком вдоволено, настав час мені припинити цей експеримент.

**Я хочу бути вільною людиною**. Фейсбук, натомість, мені трохи нагадує якусь нову релігію чи партію, до якої треба належати, щоб бути повноцінним членом суспільства. Від людини очікується, що вона «є на Фейсбуці», як і інші півтора мільярда людей. Насторожують навіть самі масштаби цієї нової «партії», як і те, що контроль над усім цим перебуває фактично в руках однієї корпорації.

**Я хочу й далі бути на зв'язку зі всіма своїми друзями, але не хочу залежати у цьому від Фейсбука**, сліпо погоджуючись на умови, які він ставить перед людьми. «Виживання» цієї мережі спирається на накопиченні безконечного масиву даних про кожного і кожну окремо, а також про всіх разом, і видобування прибутку з цих даних (цільова реклама і подібні речі). Але окрім прибутку per se, Фейсбуком керує ще один інстинкт, цілком природний і зрозумілий, — інстинкт зберігати максимальний контроль над своєю власністю (тобто користувачами).

Підозрілі «тоталітарні» замашки Фейсбука створюють стійке відчуття, що з мережами такого типу треба бути дуже обережними й не узалежнюватися від них, а замість того використовувати інші, незалежні засоби зв'язку для обміну новинами й висловлювання своїх поглядів.

Наприклад, Фейсбук [вирішує за користувачів](https://www.facebook.com/business/news/News-Feed-FYI-A-Window-Into-News-Feed), які новини їм важливо знати, а які ні, фільтрує їхній світ, аби тільки вони продовжували там перебувати й не подумали «зав'язати». Людей, які роблять підозрілі речі (наприклад, використовують ФБ під псевдонімом чи використовують якесь небажане слово у своєму дописі), можуть без попередження «вилучати» з системи, блокувати чи [цензурувати їхні дописи](http://watcher.com.ua/2015/05/19/facebook-prodovzhuye-masovo-blokuvaty-ukrayintsiv-ta-ihnoruye-porushennya-z-boku-antyukrayinskyh-korystuvachiv/). «Стандарти спільноти» у Фейсбуці встановлюються «згори», а такого поняття, як демократія, не існує. Ви або погоджуєтесь зі «стандартами» (які забороняють використання псевдонімів), або йдете (ну хіба що «поліція» ФБ ще до вас не добралася).

Фейсбук пам'ятає про своїх користувачів і користувачок і «дбає» про них навіть коли вони перебувають поза ним. Наприклад, кожна кнопка «Like», розміщена на будь-якому сайті, [повідомляє Фейсбуку](http://www.theguardian.com/technology/2015/mar/31/facebook-tracks-all-visitors-breaching-eu-law-report) про те, що та чи інша людина відвідала той чи інший сайт (до речі, ФБ слідкує так навіть за незареєстрованими відвідувач(к)ами вебсайтів — щоб цього уникнути у вебпереглядачці треба вимкнути «реп'яшки третіх сторін» (third-party cookies).

Фейсбук знає багатьох своїх користувачів «в лице» й без труднощів упізнає їх на світлинах; він також пропонує їм позначати одне одних на світлинах, щоб допомогти розширювати цю базу даних людських облич.

Приватне онлайн-спілкування між користувачами Фейсбука відбувається здебільшого за посередництвом Фейсбука — повідомлення проходять через сервери мережі, [аналізуються](http://blogs.wsj.com/digits/2012/10/03/how-private-are-your-private-messages/) й зберігаються на них; у [деяких випадках повідомлення можуть цензуруватися](http://www.wired.com/2009/05/facebooks-e-mail-censorship-is-legally-dubious-experts-say/).

Часом Фейсбук ставить на своїх користувачах експерименти, як наприклад коли [новинну стрічну близько 700 тис. людей таємно змінювали](http://www.theguardian.com/technology/2014/jun/29/facebook-users-emotions-news-feeds), а потім відслідковували, наскільки ці зміни впливали на їхню подальшу діяльність у Фейсбуці. Це все, звісна річ, цілком законно (принаймні з точки зору Фейсбука), бо користуючись цією мережею, ви даєте згоду на використання ваших даних [для дослідницьких цілей](https://www.facebook.com/policy.php).

Фейсбук [навіть помічає](http://www.slate.com/articles/technology/future_tense/2013/12/facebook_self_censorship_what_happens_to_the_posts_you_don_t_publish.html), коли ви починаєте щось писати, але передумали і вилучили текст повідомлення, так і не натиснувши кнопку «опублікувати». Фейсбук справді детально вивчає дані, які у нього є — адже доступ до думок понад мільярда людей на дорозі не валяється.

Такі мережі, як Фейсбук, використовують природне бажання людей спілкуватися й соціялізуватися для того, щоб «завербувати» якомога більше людей. Адже ж щоб надіслати користувачу чи користувачці Фейсбука повідомлення, потрібно спершу самим стати користувачами Фейсбука. На щастя, ми з Вами ще належимо до покоління, яке пам'ятає електронну пошту, а вона була створена таким чином, щоб люди могли обмінюватися листами незалежно від того, чи вони разом на одному сервері, чи на різних. Але ще трохи, і люди можуть взагалі забути, що існують якісь засоби зв'язку окрім Фейсбука.

**Щоб цього не сталося, і щоб ми й надалі могли бути вільними й незалежними людьми, нам треба сказати собі, що не конче мати обліковку на Фейсбуці, щоб бути «на зв'язку»;** що існують також інші способи спілкування й обміну новинами; способи, які не узалежнюють нас від чиєїсь монополії. Для приватних повідомлень  можна використовувати різні програми й формати, що підтримують наскрізне шифрування (наприклад, [Riot/Matrix][riot] чи [OMEMO][]); для «асинхронної» комунікації — електронну пошту ([з допомогою OpenGPG](https://ssd.eff.org/en/module/how-use-pgp-windows) можна зробити її шифрованою); для обміну новинами й думками можна скористатися [вільними й децентралізованими соцмережами](https://medium.com/@setthemfree/deletefacebook-now-what-a502b325800e#950b), базованими на ідеї незалежних, але сумісних між собою вузлів (серверів) — як і сам World Wide Web. Серед прикладів таких мереж — [Mastodon](https://uk.wikipedia.org/wiki/Mastodon), [diaspora*](https://uk.wikipedia.org/wiki/Diaspora),  [friendica](https://en.wikipedia.org/wiki/Friendica) тощо.

Будьмо вільними; наше онлайн-спілкування з друзями не повинно бути підпорядковано одній великій корпорації з дивними абміціями. Я беру [«червону таблетку»](https://en.wikipedia.org/wiki/Red_pill_and_blue_pill).

До зустрічі на свободі! :)

_Ілюстрація: [Red and blue pill](https://commons.wikimedia.org/wiki/File:Red_and_blue_pill.jpg), автор [W.carter](https://commons.wikimedia.org/wiki/User:W.carter), ліцензія [cc by-sa 4.0](http://creativecommons.org/licenses/by-sa/4.0)._

## English version
When I was registering on Facebook, I already knew I will delete my account at some point. I was curious how is it like to be on Facebook; now, when my curiosity is fully satisfied, it is time to end the experiment.

**I want to be a free person.** Facebook, instead, reminds me a bit of a new religion or party of which one must be a member to function in the society. One is expected to "be on Facebook," just as the other billion-and-a-half people. Even the scale of this new "party" is somewhat frightening, as well as the fact that the entire network is controlled by a single commercial company.

**I want to continue to be connected with all my friends, but I don't want to depend on Facebook for this**, blindly accepting its rules and conditions; the "survival" of this network is based on collection of infinite amount of data about each of its users and all of them together, and by making money out of it (through targeted advertising and the like.) But in addition to making money per se, Facebook is led by one more instinct, fully natural and understandable, the instinct to control its property (i.e. the users).

Suspicious "totalitarian" moves that Facebook makes give me a strong feeling that we should be very careful with networks of this kind and not make ourselves dependent on them. Instead, we should use other, independent means of communication and expression.

For instance, Facebook [decides on its own](https://www.facebook.com/business/news/News-Feed-FYI-A-Window-Into-News-Feed) which news are important for a user and which aren't, it filters their world to only keep them in the network and not let them think of leaving. People who do "suspicious" things (like use a pseudonym instead of real name or use an unwanted word) may be "removed" from the system, blocked or [their posts may be censored](http://watcher.com.ua/2015/05/19/facebook-prodovzhuye-masovo-blokuvaty-ukrayintsiv-ta-ihnoruye-porushennya-z-boku-antyukrayinskyh-korystuvachiv/). The "community standarts" of Facebook are provided "from the top" and there's no such thing as democracy. You either agree to the rules (which forbid using a pseudonym) or you must leave.

Facebook remembers its users and "cares" for them even when they're out of the network. For example, the "Like" button, appearing on any website, [lets Facebook know](http://www.theguardian.com/technology/2015/mar/31/facebook-tracks-all-visitors-breaching-eu-law-report) that such and such user has visited such and such website (by the way, Facebook also tracks people who don't have an account using the same omnipresent button - to block this, you should disable third-party cookies in your browser.)

Facebook knows many of its users' faces and recognizes them on photos easily; it also suggests its users to mark each other on photos to grow its database of faces.

Private online communication between Facebook users happens mostly through Facebook; messages flow through its servers, [get analyzed](http://blogs.wsj.com/digits/2012/10/03/how-private-are-your-private-messages/) and stored; in [some cases they may also be censored](http://www.wired.com/2009/05/facebooks-e-mail-censorship-is-legally-dubious-experts-say/).

Sometimes Facebook performs experiments on its users, like when [the news feed of nearly 700 thousand users was secretly manipulated](http://www.theguardian.com/technology/2014/jun/29/facebook-users-emotions-news-feeds), and then the users were monitored in order to see the effects of the manipulation on their activity. All of that is, of course, completely legal (at least in Facebook's terms), because by using Facebook [everyone agrees](https://www.facebook.com/policy.php) that their data may be used for research.

Facebook [even notices](http://www.slate.com/articles/technology/future_tense/2013/12/facebook_self_censorship_what_happens_to_the_posts_you_don_t_publish.html) when a user has started writing something, but then changed his/her mind and deleted it without pressing the "Publish" button. Facebook makes a lot of use of the data we all provide them about ourselves every time we use it.

Networks like Facebook use the natural human tendency to socialize in order to "enroll" as many people as possible - take the simple fact that to send a message to a Facebook user one has to become a Facebook user oneself. Luckily, we belong to a generation that still remembers and knows how to use email, which doesn't force everyone to be on the same server to speak to each other. But shortly we may find ourselves in a world when no one will know how to use anything besides Facebook to communicate.

**To prevent this, and to be free people, we need to decide that one doesn't have to "be on Facebook" to stay connected;** that other means of online communications, expression and sharing of information exist; that there are tools that don't make us dependent on a monopoly. For private chats one may use various programs and formats that support end-to-end encryption (examples include [Riot/Matrix][riot] and [OMEMO][]). For "asynchronous" communication you can use email (can be end-to-end encrypted [with OpenPGP](https://ssd.eff.org/en/module/how-use-pgp-windows)); for sharing of news and ideas one can use some of the [free and decentralized social networks](https://medium.com/@setthemfree/deletefacebook-now-what-a502b325800e#950b), that are based on the idea of interconnected yet independed nodes - like the original World Wide Web. Examples include [Mastodon](https://en.wikipedia.org/wiki/Mastodon_(software)),
[diaspora*](https://en.wikipedia.org/wiki/Diaspora_(social_network)),  [friendica](https://en.wikipedia.org/wiki/Friendica), etc.

Let's be free; our online communication with friends and family shouldn't be subordinated to a corporation with unhealthy ambitions. I take the "[red pill](https://en.wikipedia.org/wiki/Red_pill_and_blue_pill)."

See you in the free world.

_Image: [Red and blue pill](https://commons.wikimedia.org/wiki/File:Red_and_blue_pill.jpg) by [W.carter](https://commons.wikimedia.org/wiki/User:W.carter), [cc by-sa 4.0](http://creativecommons.org/licenses/by-sa/4.0)._

#facebook #privacy

[riot]: http://riot.im/
[OMEMO]: https://conversations.im/


