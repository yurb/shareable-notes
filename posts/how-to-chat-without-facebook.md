# How to chat with me (and any other person) without Facebook or Skype

_Instructions first._

1. Install one of the programs that support _jabber_ (also called _XMPP_, means the same thing) with OTR encryption. There's plenty, all free software:
   - Android: [Conversations](https://conversations.im/) (see [a hint below](#note-on-conversations))
   - iOS: [ChatSecure](https://chatsecure.org/)
   - Linux, Windows: [Pidgin](https://pidgin.im/) + OTR [plugin](https://otr.cypherpunks.ca/)
   - Mac: [Adium](https://www.adium.im/)
2. Pick a server from the [list of free jabber servers](https://check.messaging.one/index.php), and from within the program you've just installed, create an account on that server (account type: XMPP/jabber).
3. Boom. Add my jabber address to your contacts there or email me your jabber address, and we can chat.

## Why?
- Because this way we can have [end-to-end encryption](http://www.wired.com/2014/11/hacker-lexicon-end-to-end-encryption/), which means they won't be able to read our chats.
- Because we are free to use any program and OS we want (and it doesn't have to be _the same_ OS and program for both of us).
- Because we can even have [location anonymity](https://en.wikipedia.org/wiki/Tor_%28anonymity_network%29) if we want.
- Because we aren't forced to register on Facebook or trust our computer to closed-source programs like Skype to speak to each other.
- Because we aren't locked to a particular server or company to speak to each other.
- Because people like Ed Snowden [use it](https://firstlook.org/theintercept/2014/10/28/smuggling-snowden-secrets/).

## More info
- Detailed [step-by-step guide](https://ssd.eff.org/en/module/how-use-otr-windows) for using Pidgin with OTR on Windows.
- Detailed [step-by-step guide](https://ssd.eff.org/en/module/how-use-otr-mac) for using Adium with OTR on Mac.
- Hardcore [guide to chatting anonymously](https://firstlook.org/theintercept/2015/07/14/communicating-secret-watched/) with Tor, jabber and OTR.

## Video calls?
Sure. Install [Jitsi](https://jitsi.org/), and from within the program create a jabber (xmpp) account at jit.si server. (You may also try using the jabber account you already have, but not all jabber servers support video, so jit.si is a safer bet.)

## Note on Conversations
Despite [Conversations](https://conversations.im/) is not gratis on Google Play, it is free software (as in _freedom_) under the GPLv3 license, and you may also install it for free from [F-Droid](https://f-droid.org/packages/eu.siacs.conversations/). However, if you want to support the author, you can buy it or [donate directly](https://conversations.im/#donate).
