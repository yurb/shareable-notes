# Instead of a blog

This is my minimalistic blog-like space hosted as a public git repo at GitLab. Thanks to GitLab's ability to render markdown, it is a frontend to itself, without a need for a separate web hosting.

# TOC
See the [list of files](posts).
